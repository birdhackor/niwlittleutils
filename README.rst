NIWLittleUtils
==============
  
State: |built_state| |version_state| |license_state|

Documentation: https://niwlittleutils.readthedocs.org/

Repo: https://bitbucket.org/birdhackor/niwlittleutils

.. |built_state| image:: https://api.shippable.com/projects/542a36e280088cee586d09c4/badge?branchName=master
   :target: https://app.shippable.com/projects/542a36e280088cee586d09c4/builds/latest
.. |version_state| image:: https://img.shields.io/pypi/v/NIWLittleUtils.svg?style=flat-square
   :target: https://pypi.python.org/pypi/NIWLittleUtils
.. |license_state| image:: https://img.shields.io/pypi/l/NIWLittleUtils.svg?style=flat-square

Intro
=====
NIWLittleUtils is a tool collection that contain some useful module and function.

Read the docs at http://niwlittleutils.readthedocs.org/

This library is a work in progress.  Please give feedback!
