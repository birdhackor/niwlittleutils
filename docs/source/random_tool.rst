:mod:`random_tool` --- A module to generate things randomly.
============================================================

.. module:: random_tool
      :synopsis: A module to generate things randomly.

.. automodule:: NIWLittleUtils.random_tool
      :members:
