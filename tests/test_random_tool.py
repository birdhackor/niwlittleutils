import string
import random
try:
    from importlib import reload
except ImportError:
    from imp import reload

from NIWLittleUtils import random_tool

class TestFunction_get_random_string(object):
    def test_length(self):
        for leng in range(0, 100):
            assert leng == len(random_tool.get_random_string(leng))

    def test_allowed_chars(self):

        test_dict = {
            'complex': string.ascii_letters + string.digits + string.punctuation,
            'middle': string.ascii_letters + string.digits + '-._~',
            'simple': string.ascii_letters + string.digits,
            'lowercase': string.ascii_lowercase,
            'uppercase': string.ascii_uppercase,
            'digits': string.digits,
        }

        test_dict.update({key.upper(): value for key, value in test_dict.items()})
        test_dict.update({
            'qwerty': 'qwerty',
            '!@#$%^': '!@#$%^',
            'QAZWSXEDC': 'QAZWSXEDC'
        })

        for case, allowed_chars in test_dict.items():
            assert all(c in allowed_chars for c in random_tool.get_random_string(allowed_chars=case))

class TestFunction_get_random_string_without_using_sysrandom(TestFunction_get_random_string):
    @classmethod
    def setup_class(cls):

        def raise_NotImplementedError(cls):
            raise NotImplementedError

        random.SystemRandom.__new__ = raise_NotImplementedError
        reload(random_tool)

    @classmethod
    def teardown_class(cls):
        reload(random)
