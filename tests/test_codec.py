import os
import base64
import string

import pytest

from NIWLittleUtils import codec

class TestB64codec(object):

    def test_type(self):
        test_b = os.urandom(30)
        assert isinstance(codec.b64encode(test_b), bytes)

        test_ba = bytearray(os.urandom(30))
        assert isinstance(codec.b64encode(test_ba), bytes)

        test_b = codec.b64encode(os.urandom(30))
        assert isinstance(codec.b64decode(test_b), bytes)

        test_ba = bytearray(codec.b64encode(os.urandom(30)))
        assert isinstance(codec.b64decode(test_ba), bytes)

        test_s = codec.b64encode(os.urandom(30)).decode('ascii')
        assert isinstance(codec.b64decode(test_s), bytes)

    def test_encode_than_decode(self):
        test_b = os.urandom(30)
        assert codec.b64decode(codec.b64encode(test_b)) == test_b

class TestFunction_urlsafe_codec(object):

    def test_type(self):
        test_b = os.urandom(30)
        assert isinstance(codec.urlsafe_encode(test_b), str)

        test_s = base64.standard_b64encode(os.urandom(30)).decode()
        assert isinstance(codec.urlsafe_encode(test_s), str)

        test_s_from_b = codec.urlsafe_encode(os.urandom(30))
        assert isinstance(codec.urlsafe_decode(test_s_from_b), bytes)

        test_s_from_ba = codec.urlsafe_encode(bytearray(os.urandom(30)))
        assert isinstance(codec.urlsafe_decode(test_s_from_ba), bytes)

        test_s_from_s = codec.urlsafe_encode(
                            base64.standard_b64encode(
                                os.urandom(30)
                            ).decode()
                        )
        assert isinstance(codec.urlsafe_decode(test_s_from_s), str)

    def test_urlsafe(self):
        # RFC 3986 section 2.3
        allowed_chars = string.ascii_letters + string.digits + '-._~'

        test_b = os.urandom(60)
        assert all(c in allowed_chars for c in codec.urlsafe_encode(test_b))

    def test_compressed(self):
        test_b = os.urandom(5) * 30
        assert codec.urlsafe_encode(test_b).startswith('.')

    def test_encode_than_decode(self):
        test_b = os.urandom(60)
        assert codec.urlsafe_decode(codec.urlsafe_encode(test_b)) == test_b

        test_b = os.urandom(5) * 30
        assert codec.urlsafe_decode(codec.urlsafe_encode(test_b)) == test_b

        test_s = base64.standard_b64encode(os.urandom(30)).decode()
        assert codec.urlsafe_decode(codec.urlsafe_encode(test_s)) == test_s

    def test_error_input(self):
        with pytest.raises(ValueError) as excinfo:
            codec.urlsafe_decode('tests')
        assert 'Could not base64 decode' in str(excinfo.value)

        with pytest.raises(ValueError) as excinfo:
            codec.urlsafe_decode('.test')
        assert 'Could not zlib decompress' in str(excinfo.value)
