:mod:`filesystem` --- A module that contain some helpful function to deal with filesystem.
==========================================================================================

.. module:: filesystem
      :synopsis: A module that contain some helpful function to deal with filesystem.

.. automodule:: NIWLittleUtils.filesystem
      :members:
