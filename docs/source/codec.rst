:mod:`codec` --- A module that help handle codec.
=================================================

.. module:: codec
      :synopsis: A module that help handle codec.

.. automodule:: NIWLittleUtils.codec
      :members:

