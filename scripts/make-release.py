import os
import sys
import shutil

from pathlib import Path
from subprocess import Popen

import click

PROJECT_ROOT_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')
PYTHON_EXECUTABLE = sys.executable
TWINT_EXECUTABLE = os.path.join(os.path.dirname(sys.executable), 'twine')

@click.group()
def cli():
    pass

def git_is_clean():
    return Popen(['git', 'diff', '--quiet']).wait() == 0

@cli.command()
def build_and_upload():
    """Build sdist and wheel and then upload it to PyPI."""
    os.chdir(PROJECT_ROOT_PATH)
    returncode = Popen([sys.executable, 'setup.py', 'sdist', 'bdist_wheel']).wait()
    if returncode == 0:
        click.echo('"sdist" and "bdist_wheel" have been built.')
    else:
        sys.exit('Build failed.')
    returncode = Popen([TWINT_EXECUTABLE, 'upload'] + [str(path) for path in Path('./dist').glob('*')]).wait()
    if returncode == 0:
        click.echo('Done! "sdist" and "wheel" have been upload.')
    else:
        sys.exit('Upload failed.')

@cli.command()
@click.argument('paths', required=False, nargs=-1)
@click.option('--manual', is_flag=True,
              help=('If this flag has been added, '
                    'there won\'t be any paths by default.'))
def clean(manual, paths):
    """Remove all files which is build for PyPI or generate by testing (eg. pytest).

    Remove all files that you specify. If option ``--manual`` has been added, you will
    have to specify all paths by youself, otherwise, it will has 6 default path:
    ``*.egg-info``, ``.coverage``, ``dist``, ``build``, ``.tox``, ``.cache``. You can use ``paths`` argument
    to add more paths.

    The paths you specified should be string that can match some files or directories
    under project root directory (the parent directory of 'scripts' directory). (eg. ``**/*.txt``)
    """
    path = Path(PROJECT_ROOT_PATH)

    if manual:
        coproduct = []
    else:
        coproduct = ['*.egg-info', '.coverage', 'dist', 'build', '.tox', '.cache']

    if paths:
        coproduct = coproduct + list(paths)

    info_printed = False
    for product in coproduct:
        for p in path.glob(product):
            if not info_printed and p:
                info_printed = True
                click.echo('==================Logs==================')
            p=p.resolve()
            if p.is_dir():
                shutil.rmtree(str(p))
                click.echo('Dict "{p}" has been removed.'.format(p=p))
            elif p.is_file():
                os.remove(str(p))
                click.echo('"{p}" has been removed.'.format(p=p))
    else:
        if info_printed:
            click.echo('================End Logs================')
            click.echo('Done! All files have been cleared.')
        else:
            click.echo('Done! But there is no file have to remove.')

if __name__ == '__main__':
    cli()
