import os
try:
    from importlib import reload
except ImportError:
    from imp import reload

import pytest

from NIWLittleUtils import filesystem

class TestFunction_secure_filename(object):
    def test_some_filename(self):
        assert filesystem.secure_filename('My cool movie.mov') == 'My_cool_movie.mov'
        assert filesystem.secure_filename('cool movie.副檔名') == 'cool_movie'
        assert filesystem.secure_filename('"cool movie.mov"; "../../../etc/passwd"') == 'cool_movie.mov_.._.._.._etc_passwd'
        assert filesystem.secure_filename('../../../etc/passwd') == 'etc_passwd'
        assert filesystem.secure_filename('etc:passwd') == 'etcpasswd'
        assert filesystem.secure_filename(r'//etc:passwd') == 'etcpasswd'
        assert filesystem.secure_filename(r'\\etc:passwd') == 'etcpasswd'
        assert filesystem.secure_filename('i contain cool \xfcml\xe4uts.txt') == \
        'i_contain_cool_ümläuts.txt'
        assert filesystem.secure_filename('i contain cool \xfcml\xe4uts.txt', 'ascii') == \
        'i_contain_cool_umlauts.txt'

        with pytest.raises(ValueError) as excinfo:
            filesystem.secure_filename('My cool movie.mov', 'big5')
        assert 'Argument ``codec`` should be *utf8* or *ascii*.' in str(excinfo.value)

        with pytest.raises(TypeError) as excinfo:
            filesystem.secure_filename(333)
        assert 'Filename should be a instance of str.' in str(excinfo.value)

class TestFunction_secure_filename_force_nt(TestFunction_secure_filename):
    def test_windows_device_files(self):
        for d in ('CON', 'AUX', 'COM1', 'COM2', 'COM3', 'COM4', 'LPT1',
            'LPT2', 'LPT3', 'PRN', 'NUL'):
            assert filesystem.secure_filename(d) == '_' + d

    @classmethod
    def setup_class(cls):
        os.name = 'nt'
        reload(filesystem)

    @classmethod
    def teardown_class(cls):
        reload(os)

class TestFunction_scantree(object):
    def test_defult_path(self, tmpdir):
        folder_top = tmpdir.mkdir('top_folder')
        second_folder = folder_top.mkdir('second_folder')
        third_folder = second_folder.mkdir('third_folder')
        test1 = second_folder.join('test1.txt')
        test1.write('ddd')
        test2 = third_folder.join('test2.txt')
        test2.write('qqq')
        old_dir = folder_top.chdir()
        tree = sorted(filesystem.scantree(), key=lambda f: f.path)
        assert os.stat(second_folder.strpath) == tree[0].stat()
        assert os.stat(test1.strpath) == tree[1].stat()
        assert os.stat(third_folder.strpath) == tree[2].stat()
        assert os.stat(test2.strpath) == tree[3].stat()
        old_dir.chdir()

    def test_path(self, tmpdir):
        folder_top = tmpdir.mkdir('top_folder')
        second_folder = folder_top.mkdir('second_folder')
        third_folder = second_folder.mkdir('third_folder')
        test1 = second_folder.join('test1.txt')
        test1.write('ddd')
        test2 = third_folder.join('test2.txt')
        test2.write('qqq')
        tree = sorted(filesystem.scantree(folder_top.strpath), key=lambda f: f.path)
        assert os.stat(second_folder.strpath) == tree[0].stat()
        assert os.stat(test1.strpath) == tree[1].stat()
        assert os.stat(third_folder.strpath) == tree[2].stat()
        assert os.stat(test2.strpath) == tree[3].stat()
